package fr.univlille.iut.r3_04.tp2;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Felix {
	
	void drawHead(GraphicsContext gc){
		gc.setFill(Color.GREY);
		//int x, int y, int width, int height, int arcWidth, int arcHeight
		gc.fillOval(50, 30, 180, 200);
	}

	void drawEyes(GraphicsContext gc) {
		gc.setFill(Color.BLUE);
		gc.setStroke(Color.YELLOW);
		gc.fillRoundRect(100, 80, 25, 50, 20, 100);
		gc.fillRoundRect(150, 80, 25, 50, 20, 100);
	}
	
	void drawMouth(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.setStroke(Color.YELLOW);
		gc.fillOval(100, 160, 75, 25);
	}
	
	public void drawOn(GraphicsContext gc) {
		drawHead(gc);
		drawWiskers(gc);
		drawEyes(gc);
		drawMouth(gc);
		drawEars(gc);
	}
	
	public void drawEars(GraphicsContext gc) {
		gc.setFill(Color.LIGHTGRAY);
		gc.fillPolygon(new double[] {90,70,140},new double[] {65,10,60} , 3);
		gc.setFill(Color.LIGHTGRAY);
		gc.fillPolygon(new double[] {160,230,210},new double[] {60,10,65} , 3);
	}
		
	public void drawWiskers(GraphicsContext gc){
        gc.strokeLine(45,100,130,150);
        gc.strokeLine(45,75,130,150);
        gc.strokeLine(255,75,170,150);
        gc.strokeLine(255,100,170,150);
	}

}
